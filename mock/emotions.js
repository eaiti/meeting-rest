const fetch = require('node-fetch');
// const mockTranscriptions = require('./engage-handoff');
// const mockTranscriptions = require('./plg-demo');
// const mockTranscriptions = require('./tote-meeting');
const mockTranscriptions = require('./yusen-discovery');

const transcript = mockTranscriptions.transcript;

const getEmotions = async transcript => {
    const transcriptMap = new Map(); // time -> text

    transcript.forEach(segment => {
        const minute = Math.round(segment.startTime / 60);
        if (transcriptMap.has(minute)) {
            transcriptMap.set(minute, transcriptMap.get(minute) + ' ' + segment.text)
        } else {
            transcriptMap.set(minute, segment.text);
        }
    });

    const emotionMap = {}; // time -> emotion
    for ([time, text] of transcriptMap) {
        await delay(100); // API doesn't like being spammed.

        const response = await fetch(`http://localhost:3000/emotions?text=${text}`);
        const data = await response.json();

        emotionMap[time] = data;
        console.log(data);
    }

    return emotionMap;
};

const printEmotionMap = async transcript => {
    const emotionMap = await getEmotions(transcript);
    console.log('------------------------');
    console.log(JSON.stringify(emotionMap));
}

const delay = ms => new Promise(r => setTimeout(r, ms));

printEmotionMap(transcript);
