const mockTranscriptions = require('./engage-handoff');
// const mockTranscriptions = require('./plg-demo');
// const mockTranscriptions = require('./tote-meeting');
// const mockTranscriptions = require('./yusen-discovery');

const actionItems = ["Hold on.", "Let's see if I typed that correctly.", "Let's try that yet.", "So let's just verifies there.", "So let's go back to engagements in the top, right.", "Just hit the x here.", "Let's see if I can do this on my bill screen here.", "Go to the old contagion on the ones like anxiety, like as much as possible on honestly, yeah, I've noticed on your engage <inaudible> user dash one one at <inaudible> dot com so like how much of this you think it would be to make some for same thing.", "Now let's go back to research <inaudible>.", "So let's go to create entity.", "So let's go to the top left and select a phase.", "Hold on a second.", "Do this.", "Let me rename it on a second.", "Can probably fill out an email file template ourselves if we really need to make shifts.", "Just want to see the things like the tshirt and classroom time shirt.", "Let go.", "See how I did it.", "Should have been refreshed but you can just reload it looks like we have that in a couple of places.", "Let's try another one I guess.", "Let's see what happens.", "Let's see.", "Forget what I said.", "Cause it might not be something very specific to what they're doing and what we're looking at that thing that's like, oh well, okay.", "Do whatever you guys got to do to credentials."];

const questions = ["<inaudible> does that work?", "Exactly?", "What are loading here eventually?", "Zach, what's the password again?", "Do you think any fun aboriginal words?", "It's though, how about Afghanistan?", "That's good, right?", "And I'm sorry, is that okay?", "Huh?", "All right, we good?", "Where should I go now?", "You guys have any questions about any of the stuff that we just did?", "Are there different levels of permissions for the piece?", "Will you add to the different phases?", "<inaudible> Huh?", "So can we do time plates on the stick with some like the secondary terraform system or whatever?", "Anything else I can do for it?", "<inaudible> <inaudible> and what was the question before about kind of setup scripts?", "Is that right?", "And where does it, cause he's always, which camera is <inaudible> user going to instead of username?", "I mean it's configurable because if there's two users connected, how are you going to know which users connecting?", "Well how would the key, how would the ssh keys work?", "I was like, all right, or do we need this a ssh session anymore?", "So if you click on the email drop down, but for fish, we wouldn't necessarily know a host at that, right?", "Is there a way to record once we sent out that don't want and don't target?", "So what am I doing here?", "Did I go too far?", "Right?", "Does this happen to call it dot EML file also checks the extension as well as the content type from the byte screen transferred?", "So how much of this stuff is required here?", "I mean as what, what's the difference between servers and the name?", "I guess?", "So if we have a one like that and we close it out and then we decided to go back to it later as can we reopen that?", "Uh, what did you say, Matt?", "What are some things that we want?", "Oh, it's, there's nothing in it?", "Is that like inclusive and like the timestamps in the international center like that?", "Are we able to upload like a CSV with some of these things just in the top right?", "Is there one for now on?", "For like fishing?", "What's this written in from the back end?", "What's the question?", "Uh, did you want me to create another attempt then to know?", "Um, where do I go for that?", "Hello?", "And then where, and then we're supposed to go run the script or is there, does it run automatically for us?", "Yeah, it is, Huh?", "So like an Admin perspective?", "Or is it just within seconds?", "Can you just close this?", "Is that something that we can do in this released here?", "And this is the kind of Omni search thing, right?", "It would tell us if that one was already taken, right?", "How's your Google competing stuff coming along?", "What was the question?", "How's the Google compute?", "Which one did you mean for domain?", "From Tim?", "I don't know how we want to represent that in the system with this theater type, right?", "And so this would be where management would come to take control and curate this list and assign them to projects?", "And if I'm like them or the regular user, can I do that as well?", "The here?", "Anything else?", "Can you use the searching database as your primary database?", "Well, it looks like the phase weirdly doesn't, is that maybe the name thing I failed to fill out or something?", "Do you know where they put the API?", "Uh, well that's, that's exactly pushing me for API, um, that do we have an external facing API on the roadmap?", "Are they all the same?"]

const transcript = mockTranscriptions.transcript;

const getTime = (transcript, text) => {
    const segment = transcript.find(t => {
        return t.text.indexOf(text) !== -1;
    });

    return segment;
}

const getTimes = (transcript, texts) => {
    const times = [];
    texts.forEach(text => {
        const segment = getTime(transcript, text);
        if (segment) {
            times.push(`[${getMinute(segment.startTime)}-${getMinute(segment.endTime)}] ${text}`);
        }
    });

    console.log(JSON.stringify(times));
}

const getMinute = time => {
    const seconds = time / 60

    let minute = Math.floor(seconds);
    const second = Math.round(seconds / 60 % 1 * 60);

    return second < 10 ? `${minute}:0${second}` : `${minute}:${second}`;
}

getTimes(transcript, actionItems);
console.log('-------------');
getTimes(transcript, questions);
