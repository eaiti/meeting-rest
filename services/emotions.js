const fetch = require('node-fetch');
const Sentiment = require('sentiment');

const getEmotions = text => {
    return new Promise((resolve, reject) => {
        const url = `https://www.twinword.com/api/v7/emotion/analyze/?text=${text}`

        fetch(url, { method: 'POST' })
            .then(res => res.json())
            .then(data => resolve({
                detected: data['emotions_detected'],
                scores: data['emotion_scores'],
            }))
            .catch(err => reject(err));
    });
};

const getEmotionalWords = text => {
    return new Promise((resolve, reject) => {
        const sentiment = new Sentiment();

        try {
            const analysis = sentiment.analyze(text);

            resolve({
                positive: [...new Set(analysis.positive)].sort(),
                negative: [...new Set(analysis.negative)].sort(),
            });
        } catch (err) {
            reject(err);
        }
    });
};

module.exports = {
    getEmotions,
    getEmotionalWords,
}
