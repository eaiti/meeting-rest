const fetch = require('node-fetch');
const apiKeys = require('../api-keys.json');
const FormData = require('form-data');
const { execSync } = require('child_process');

async function startTranscriptionJob(videoLocation) {
//   // const url = `curl -X POST "https://api.rev.ai/speechtotext/v1/jobs" -H "Authorization: Bearer ${apiKeys.revApiKey}" -H "Content-Type: multipart/form-data" -F "media=@${videoLocation};type=audio/mp4" -F "options={\"metadata\":\"This is a sample submit jobs option for multipart\"}"`;
//   console.log('trascribing video at', videoLocation);
//   const url = "https://api.rev.ai/speechtotext/v1/jobs";
//   const headers = { 'Authorization': `Bearer ${apiKeys.revApiKey}`, 'Content-Type': 'multipart/form-data'};
//   const form = new FormData();
//   form.append('media', `@${videoLocation};type=audio/mp4`);
//   // form.append('media', `@${videoLocation}`);
//   // form.append('type', 'audio/mp4');
//   form.append('options', `{"metadata": "This is a sample submit jobs option for multipart"}`);
//   console.log('FORMFORM', form);

//   const response = await fetch(url, { method: 'POST', headers, body: form });
//   const job = await response.json();
  // return job;
}

async function getListOfJobs() {
  const headers = { 'Authorization': `Bearer ${apiKeys.revApiKey}` };
  const url = "https://api.rev.ai/speechtotext/v1/jobs?limit=100";
  const response = await fetch(url, { method: 'GET', headers });
  const jobs = await response.json();
  return jobs;
}

async function getTranscript(id) {
  const headers = { 'Authorization': `Bearer ${apiKeys.revApiKey}` };
  const url = `https://api.rev.ai/speechtotext/v1/jobs/${id}/transcript`;
  const response = await fetch(url, { method: 'GET', headers });
  const transcript = await response.json();
  return convertTranscript(transcript);
}

function convertTranscript(transcript) {
  const transformedTranscript = [];
  for (const monologue of transcript.monologues) {
    const startTs = Math.min(...monologue.elements.filter(e => !!e.ts).map(e => e.ts));
    const endTs = Math.max(...monologue.elements.filter(e => !!e.end_ts).map(e => e.end_ts));
    transformedTranscript.push(
    {
      speaker: monologue.speaker,
      startTime: startTs,
      endTime: endTs,
      text: monologue.elements.reduce((acc, e) => acc.concat(e.value), ''),
    })
  }
  return transformedTranscript;
}
exports.startTranscriptionJob = startTranscriptionJob;
exports.getListOfJobs = getListOfJobs;
exports.getTranscript = getTranscript;