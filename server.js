const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

const emotionService = require('./services/emotions');
const transcriptionService = require('./services/transcription');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/emotions', async (req, res) => {
  if (!req.query || !req.query.text) {
    res.status(400).send('Invalid input.');
    return;
  }

  try {
    const text = req.query.text;
    const [emotions, emotionalWords] = await Promise.all([
      emotionService.getEmotions(text),
      emotionService.getEmotionalWords(text),
    ]);

    res.send({
      emotions,
      positive: emotionalWords.positive,
      negative: emotionalWords.negative,
    });
  } catch (err) {
    res.status(500).send(err);
  }
});

app.post('/transcribe', async (req, res) => {
  if (!req.body || !req.body.location) {
    res.status(400).send('Invalid input.');
    return;
  }

  console.log('STARTING TO TRANSCRIBE');

  const job = await transcriptionService.startTranscriptionJob(req.body.location);
  res.send({
    job,
  });
});

app.get('/jobs', async (req, res) => {
  const jobs = await transcriptionService.getListOfJobs();
  res.send({
    jobs,
  });
});

app.get('/transcript/:id', async (req, res) => {
  const transcript = await transcriptionService.getTranscript(req.params.id);
  res.send({
    transcript,
  });
});


app.get('/report', (req, res) => res.send('This is the report!'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));