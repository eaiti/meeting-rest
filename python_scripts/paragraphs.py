import bs4 as bs  
import urllib
import re
from load_text import loadMeetingText
from nltk.tokenize import TextTilingTokenizer

def getParagraphs(meeting_text):
  # 1. Tokenizing into paragraphs
  ttt = TextTilingTokenizer()
  tokens = ttt.tokenize(meeting_text)
  paragraph_list = [k.replace("\n", " ") for k in tokens]
  return paragraph_list

  