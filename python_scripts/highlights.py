from paragraphs import getParagraphs
from summary import getSummary

def getHighlights(meeting_text):
  # 1. Get into paragraphs
  paragraphs = getParagraphs(meeting_text)

  highlights = []
  # 2. Get summary of each paragraph
  for paragraph in paragraphs:
    partial_summary = getSummary(paragraph)
    highlights.append(partial_summary)

  return highlights