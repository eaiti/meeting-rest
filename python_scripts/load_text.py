# Fetching Meeting Text from the given input.

import bs4 as bs
import re

def loadMeetingText(scraped_data):
  meeting = scraped_data.read()

  parsed_meeting = bs.BeautifulSoup(meeting,'lxml')

  paragraphs = parsed_meeting.find_all('p')

  meeting_text = ""

  for p in paragraphs:  
    meeting_text += p.text
      
  return meeting_text