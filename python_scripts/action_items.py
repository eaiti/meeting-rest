import urllib
import nltk
import re
from load_text import loadMeetingText
from imperative_detection import is_imperative

def getActionItems(meeting_text):
  # 1. Removing Square Brackets and Extra Spaces
  meeting_text = re.sub(r'\[[0-9]*\]', ' ', meeting_text)  
  meeting_text = re.sub(r'\s+', ' ', meeting_text)  

  # 2. Converting Text To Sentences
  sentence_list = nltk.sent_tokenize(meeting_text)

  imperative_list = []
  # 3. Check if a sentence is imperative
  for sentence in sentence_list:
    tokens = nltk.word_tokenize(sentence)
    tagged_sentence = nltk.pos_tag(tokens)
    is_imperative_sentence = is_imperative(tagged_sentence)
    if is_imperative_sentence:
      imperative_list.append(sentence)
    
  return imperative_list