import re
import nltk

def getQuestions(meeting_text):
  # 1. Removing Square Brackets and Extra Spaces
  meeting_text = re.sub(r'\[[0-9]*\]', ' ', meeting_text)  
  meeting_text = re.sub(r'\s+', ' ', meeting_text)  

  # 2. Removing special characters and digits
  formatted_meeting_text = re.sub('[^a-zA-Z]', ' ', meeting_text )  
  formatted_meeting_text = re.sub(r'\s+', ' ', formatted_meeting_text) 

  # 3. Converting Text To Sentences
  sentence_list = nltk.sent_tokenize(meeting_text)

  # 5. Filtering the questions
  question_list = [k for k in sentence_list if '?' in k]
  return question_list