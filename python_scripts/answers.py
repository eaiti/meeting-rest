import bs4 as bs  
import urllib
import re
from nltk.tokenize import TextTilingTokenizer
from load_text import loadMeetingText
from questions import getQuestions
from paragraphs import getParagraphs

def getAnswers(meeting_text):
  # 1. Tokenizing into paragraphs
  paragraphs = getParagraphs(meeting_text)

  # 2. Get questions
  questions = getQuestions(meeting_text)

  # 3. Loop through the paragraphs list and match the questions to get answers
  question_answer_dict = {}
  for question in questions:
    if question in paragraphs:
      index = paragraphs.index(question)
      question_answer_dict[question] = paragraphs[index]
  
  return question_answer_dict